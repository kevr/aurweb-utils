# aurweb-utils

A collection of utility scripts used to manage
[aurweb](https://gitlab.archlinux.org/archlinux/aurweb).
All scripts lie within the [./scripts](./scripts) directory and are not tied
to upstream whatsoever.

These utilities should be seen as personal utilities at this point; most of
them were made out of personal need for automation by Kevin
[kevr](https://gitlab.archlinux.org/kevr) Morris.

# Dependencies

This project leverages the existing `gitlab-tools` repository hosted by GitHub
by Kevin Morris. To install submodules, run the following:

```
$ git submodule update --init
```

`gitlab-tools` must be installed via `pip install .` inside its directory
before any scripts in this repository can run.

```
aurweb-utils$ cd gitlab-tools
gitlab-tools$ pip install .
```

To uninstall `gitlab-tools` at any time, issue `pip uninstall gitlab-tools`
and follow the prompts.

# Authors

| Name         | Email          |
|--------------|----------------|
| Kevin Morris | kevr@0cost.org |

# License

This project operates under the MIT Public License. See [./LICENSE](./LICENSE)
for full details.
