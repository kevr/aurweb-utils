#!/bin/bash
# Print out missing models from the `aurweb` Python package.
# This script deals with this process by first, fetching our tables
# out of MySQL, then checking to see if they exist somewhere in
# aurweb/models/*.py.

# Get all tables with the exception of `alembic_version`; exclude
# top line from MySQL query result: `Tables_in_aurweb`.
tables="$(mysql -uaur -paur aurweb -e 'SHOW TABLES' \
    | awk '{print $1}' \
    | grep -v Tables_in_aurweb \
    | grep -v alembic_version)"

for table in $tables; do
    if ! grep -q $table aurweb/models/*.py; then
        echo "$table"
    fi
done
