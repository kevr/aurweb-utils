#!/bin/bash
# Creates issues for every model which is found to be missing
# from the aurweb Python package.

SCRIPTDIR="$(dirname $0)"
MODELS="$(bash ${SCRIPTDIR}/missing_models.sh)"
TEMPLATE="${SCRIPTDIR}/../templates/Issue.md"

source ./.env
export GITLAB_URL
export GITLAB_TOKEN
export GITLAB_CLIPPER

for model in $MODELS; do
    value="s;VALUE;${model};g"
    gitlab-issue-create -r archlinux/aurweb \
        -s "[DB] $model SQLAlchemy ORM model" \
        -b "$(sed -r $value $TEMPLATE)" \
        --labels next-up database python fastapi \
        --milestone 'Python/FastAPI'
    echo "Created issue for '$model'."
done
