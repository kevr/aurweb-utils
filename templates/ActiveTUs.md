There is an issue with our `ActiveTUs` column of the
`TU_VoteInfo` table.

First off, I'm not sure it makes much sense to restrict
the number of `ActiveTUs` to < 3 digits: `aurweb.schema.TU_VoteInfo`.

`TINYINT(3)` is used here, where we could use another integer type
which supports more than three digits in it.

Of course, some people will say, hey man, that's in production, we're
never going to have >= 1000 TUs active on VoteInfo. I say, that's a
silly assumption and there's not much reason to restrict it this much.

The plan is to change `TU_VoteInfo.ActiveTUs` type to an integer type
which supports more than three digits.
