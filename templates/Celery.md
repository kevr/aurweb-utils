There are various tables in the `aurweb` database which need to
be routinely pruned; things like `Sessions` and `Bans`. There may
be more.

We can use the `celery` framework to schedule pruning jobs for
our web application.

This issue is being setup to track the progress or conversation on
this topic. We should have a complete set of tables to prune before
we work on implementing this.
