We need a `VALUE` SQLAlchemy ORM model in the `aurweb` package
toward our efforts for the FastAPI port.

A simple example model can be found at `aurweb/models/term.py`.
